FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY build/libs/*.jar coliving-docker-0.1.0.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/coliving-docker-0.1.0.jar"]
EXPOSE 8080

