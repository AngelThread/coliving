
--Drop DDLs if they exist
DROP TABLE IF EXISTS room_bookings;
DROP TABLE IF EXISTS booking;
DROP TABLE IF EXISTS room;
DROP SEQUENCE IF EXISTS  hibernate_sequence;

CREATE EXTENSION IF NOT EXISTS btree_gist;
create sequence IF NOT EXISTS hibernate_sequence start with 1 increment by 1;
create table IF NOT EXISTS booking (id bigint not null, booking_interval daterange not null CHECK ( isEmpty(booking_interval)=false ),room_id bigint not null , EXCLUDE USING GIST (room_id with= ,booking_interval  WITH &&), primary key (id));
create table IF NOT EXISTS room (id bigint not null, room_type varchar(255) not null, primary key (id));
create table IF NOT EXISTS room_bookings (room_id bigint not null, bookings_id bigint not null, primary key (room_id, bookings_id));

alter table room_bookings add constraint room_bookings_id unique (bookings_id);
alter table room_bookings add constraint room_bookings_booking_id foreign key (bookings_id) references booking;
alter table room_bookings add constraint room_bookings_room_id foreign key (room_id) references room;
