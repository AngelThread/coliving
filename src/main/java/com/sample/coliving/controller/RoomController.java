package com.sample.coliving.controller;

import com.sample.coliving.controller.dto.RoomDTO;
import com.sample.coliving.controller.entity.RoomDO;
import com.sample.coliving.controller.resource.RoomResource;
import com.sample.coliving.controller.resource.RoomResourceAssembler;
import com.sample.coliving.exception.EntityNotFoundException;
import com.sample.coliving.mapper.RoomMapper;
import com.sample.coliving.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * All operations with a room will be routed by this controller.
 * <p/>
 */
@RestController
@RequestMapping("v1/rooms")
public class RoomController {

    @Autowired
    private RoomService roomService;
    @Autowired
    private RoomMapper roomMapper;
    @Autowired
    private RoomResourceAssembler roomResourceAssembler;

    @GetMapping(value = "/{room_id}", produces = {"application/hal+json"})
    public RoomResource getRoom(@PathVariable(name = "room_id") long roomId) throws EntityNotFoundException {

        RoomDO roomDO = roomService.getRoom(roomId);
        RoomDTO roomDTO = roomMapper.roomDOToRoomDTO(roomDO);
        return roomResourceAssembler.toResource(roomDTO);
    }

    @GetMapping(value = "", produces = {"application/hal+json"})
    public List<RoomResource> getAllRooms() {

        List<RoomDO> allRooms = roomService.getAllRooms();
        List<RoomDTO> roomDTOList = allRooms.stream()
                .map(roomDO -> roomMapper.roomDOToRoomDTO(roomDO))
                .collect(Collectors.toList());

        return roomResourceAssembler.toResources(roomDTOList);
    }

    @PostMapping(value = "", produces = {"application/hal+json"})
    public RoomResource createRoom(@Valid @RequestBody RoomDTO roomDTO) {
        RoomDO receivedRoomRequest = roomMapper.roomDTOToRoomDO(roomDTO);
        RoomDO createdRoom = roomService.createRoom(receivedRoomRequest);
        RoomDTO createdRoomDTO = roomMapper.roomDOToRoomDTO(createdRoom);
        return roomResourceAssembler.toResource(createdRoomDTO);
    }

}
