package com.sample.coliving.controller;

import com.sample.coliving.controller.dto.BookingDTO;
import com.sample.coliving.controller.entity.BookingDO;
import com.sample.coliving.controller.resource.BookingResource;
import com.sample.coliving.controller.resource.BookingResourceAssembler;
import com.sample.coliving.exception.ConstraintsViolationException;
import com.sample.coliving.exception.EntityNotFoundException;
import com.sample.coliving.exception.RoomAlreadyTakenException;
import com.sample.coliving.mapper.BookingMapper;
import com.sample.coliving.service.BookingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * All operations related to bookings will be routed by this controller.
 * <p/>
 */
@RestController
@RequestMapping("v1/bookings")
@Slf4j
public class BookingController {
    @Autowired
    private BookingService bookingService;
    @Autowired
    private BookingMapper bookingMapper;
    @Autowired
    private BookingResourceAssembler bookingResourceAssembler;

    @GetMapping(value = "", produces = {"application/hal+json"})
    @ResponseStatus(HttpStatus.OK)
    public List<BookingResource> getAllBookings() {

        List<BookingDO> createdBooking = bookingService.getAllBookings();
        List<BookingDTO> bookingDTOList = createdBooking.stream()
                .map(bookingDO -> bookingMapper.bookingDOToBookingDTO(bookingDO))
                .collect(Collectors.toList());

        return bookingResourceAssembler.toResources(bookingDTOList);
    }

    @PostMapping(value = "", produces = {"application/hal+json"})
    @ResponseStatus(HttpStatus.CREATED)
    public BookingResource createBooking(@Valid @RequestBody BookingDTO bookingDTO) throws ConstraintsViolationException {
        BookingResource bookingResource = null;
        try {
            BookingDO bookingDO = bookingMapper.bookingDTOToBookingDo(bookingDTO);
            BookingDO createdBooking = bookingService.createBooking(bookingDO);
            BookingDTO createdDTO = bookingMapper.bookingDOToBookingDTO(createdBooking);
            bookingResource = bookingResourceAssembler.toResource(createdDTO);
        } catch (RoomAlreadyTakenException ex) {
            log.debug("There is already a booking!", ex);
            throw new ConstraintsViolationException("There is already a booking!");
        }
        return bookingResource;
    }

    @DeleteMapping(value = "/{booking_id}", produces = {"application/hal+json"})
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Void> deleteBooking(@PathVariable(name = "booking_id") long bookingId) throws EntityNotFoundException {
        bookingService.deleteBooking(bookingId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/{booking_id}", produces = {"application/hal+json"})
    @ResponseStatus(HttpStatus.OK)
    public BookingResource getBooking(@PathVariable(name = "booking_id") long bookingId) throws EntityNotFoundException {
        BookingDO booking = bookingService.findBooking(bookingId);
        BookingDTO createdDTO = bookingMapper.bookingDOToBookingDTO(booking);
        return bookingResourceAssembler.toResource(createdDTO);

    }
}
