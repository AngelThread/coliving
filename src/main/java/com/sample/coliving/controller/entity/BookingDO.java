package com.sample.coliving.controller.entity;

import com.vladmihalcea.hibernate.type.range.PostgreSQLRangeType;
import com.vladmihalcea.hibernate.type.range.Range;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.TypeDef;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "booking")
@Data
@TypeDef(
        typeClass = PostgreSQLRangeType.class,
        defaultForType = Range.class
)
@NoArgsConstructor
public class BookingDO {

    @Id
    @GeneratedValue
    private Long id;

    @NonNull
    @Column(name="room_id")
    private Long roomId;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(
            name = "booking_interval",
            columnDefinition = "daterange"
    )
    private Range<LocalDate> bookingInterval;


}
