package com.sample.coliving.controller.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.Identifiable;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class BookingDTO implements Identifiable<Long> {
    @JsonIgnore
    @Getter(AccessLevel.NONE)
    private Long id;

    @NotNull(message = "Room id can not be null!")
    private Long roomId;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @NotNull(message = "Start date can not be null!")
    private LocalDate startDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @NotNull(message = "End date can not be null!")
    private LocalDate endDate;

    @JsonIgnore
    public Long getId() {
        return this.id;
    }
}
