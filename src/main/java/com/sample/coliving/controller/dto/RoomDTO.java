package com.sample.coliving.controller.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sample.coliving.controller.common.RoomType;
import lombok.Data;
import org.springframework.hateoas.Identifiable;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Data
public class RoomDTO implements Identifiable<Long> {
    @JsonIgnore
    private Long id;

    @NotNull(message = "Room type can not be null!")
    private RoomType roomType;

    @JsonIgnore
    private List<LocalDate> bookingCalendar;


}
