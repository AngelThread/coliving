package com.sample.coliving.controller.resource;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.ResourceSupport;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class BookingResource extends ResourceSupport {
    @Getter(AccessLevel.NONE)
    private Long id;

    private Long roomId;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate startDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate endDate;

    @JsonGetter(value = "id")
    public long getIdField() {
        return this.id;
    }
}
