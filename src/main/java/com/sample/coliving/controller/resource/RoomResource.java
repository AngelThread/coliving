package com.sample.coliving.controller.resource;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sample.coliving.controller.common.RoomType;
import lombok.*;
import org.springframework.hateoas.ResourceSupport;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class RoomResource extends ResourceSupport {
    @Getter(AccessLevel.NONE)
    private Long id;

    private RoomType roomType;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<LocalDate> bookingCalendar =new ArrayList<>();

    @JsonGetter(value = "id")
    public long getIdField() {
        return this.id;
    }

}
