package com.sample.coliving.controller.resource;

import com.sample.coliving.controller.BookingController;
import com.sample.coliving.controller.dto.BookingDTO;
import com.sample.coliving.exception.EntityNotFoundException;
import com.sample.coliving.mapper.BookingResourceMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
@Slf4j
public class BookingResourceAssembler extends ResourceAssemblerSupport<BookingDTO, BookingResource> {

    private BookingResourceMapper bookingResourceMapper;

    public BookingResourceAssembler(@Autowired BookingResourceMapper bookingResourceMapper) {
        super(BookingController.class, BookingResource.class);
        this.bookingResourceMapper = bookingResourceMapper;
    }


    @Override
    public BookingResource toResource(BookingDTO bookingDTO) {
        if (Objects.isNull(bookingDTO)) {
            return null;
        }

        BookingResource roomResource = bookingResourceMapper.bookingDTOToBookingResource(bookingDTO);
        Link selfRel = linkTo(BookingController.class).slash(bookingDTO).withSelfRel();
        roomResource.add(selfRel);
        Link allBookings = linkTo(methodOn(BookingController.class).getAllBookings()).withRel("allBookings");
        roomResource.add(allBookings);
        try {
            Link deleteBooking = linkTo(methodOn(BookingController.class).deleteBooking(bookingDTO.getId())).
                    withRel("deleteBooking");
            roomResource.add(deleteBooking);
        } catch (EntityNotFoundException e) {
            log.error("Resource building error!", e);
        }

        return roomResource;
    }
}
