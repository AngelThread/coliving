package com.sample.coliving.controller.resource;

import com.sample.coliving.controller.RoomController;
import com.sample.coliving.controller.dto.RoomDTO;
import com.sample.coliving.mapper.RoomResourceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class RoomResourceAssembler extends ResourceAssemblerSupport<RoomDTO, RoomResource> {

    private RoomResourceMapper roomResourceMapper;

    /**
     * @param roomResourceMapper
     */
    public RoomResourceAssembler(@Autowired RoomResourceMapper roomResourceMapper) {
        super(RoomController.class, RoomResource.class);
        this.roomResourceMapper = roomResourceMapper;
    }

    /**
     * @param roomDTO
     * @return
     */
    @Override
    public RoomResource toResource(RoomDTO roomDTO) {
        RoomResource roomResource = roomResourceMapper.roomDTOToRoomResource(roomDTO);
        Link selfRel = linkTo(RoomController.class).slash(roomDTO).withSelfRel();
        roomResource.add(selfRel);
        Link allRooms = linkTo(methodOn(RoomController.class).getAllRooms()).withRel("allRooms");
        roomResource.add(allRooms);

        return roomResource;
    }
}
