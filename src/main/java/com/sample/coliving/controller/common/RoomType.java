package com.sample.coliving.controller.common;

/**
 * Possible room types
 */
public enum  RoomType {
    SINGLE,DOUBLE,SUITE
}
