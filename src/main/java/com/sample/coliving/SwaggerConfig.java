package com.sample.coliving;

import com.fasterxml.classmate.TypeResolver;
import com.google.common.base.Predicates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalDate;
import java.util.List;

import static springfox.documentation.schema.AlternateTypeRules.newRule;

@EnableSwagger2
@Configuration
public class SwaggerConfig {


    @Bean
    public Docket swagger(@Autowired TypeResolver typeResolver) {
        return new Docket(DocumentationType.SWAGGER_2)//<3>
                .alternateTypeRules(
                        newRule(typeResolver.resolve(List.class, LocalDate.class),
                                typeResolver.resolve(List.class, String.class)))
                .directModelSubstitute(LocalDate.class, String.class)
                .select()//<4>
                .apis(RequestHandlerSelectors.any())//<5>
                .paths(Predicates.not(PathSelectors.regex("/error.*")))//<6>, regex must be in double quotes.
                .build();

    }
}