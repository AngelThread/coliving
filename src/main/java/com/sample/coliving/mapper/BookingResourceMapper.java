package com.sample.coliving.mapper;

import com.sample.coliving.controller.dto.BookingDTO;
import com.sample.coliving.controller.resource.BookingResource;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface BookingResourceMapper {

    @Mapping(target = "links", ignore = true)
    BookingResource bookingDTOToBookingResource(BookingDTO bookingDTO);
}
