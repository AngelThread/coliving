package com.sample.coliving.mapper;


import com.sample.coliving.controller.dto.RoomDTO;
import com.sample.coliving.controller.resource.RoomResource;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface RoomResourceMapper {

    @Mapping(target = "links", ignore = true)
    RoomResource roomDTOToRoomResource(RoomDTO roomDTO);
}
