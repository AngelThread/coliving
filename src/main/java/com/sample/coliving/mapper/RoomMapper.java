package com.sample.coliving.mapper;

import com.sample.coliving.controller.dto.RoomDTO;
import com.sample.coliving.controller.entity.BookingDO;
import com.sample.coliving.controller.entity.RoomDO;
import com.vladmihalcea.hibernate.type.range.Range;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Mapper(componentModel = "spring")
public abstract class RoomMapper {

    @Mapping(target = "bookings", ignore = true)
    public abstract RoomDO roomDTOToRoomDO(RoomDTO roomDTO);

    @Mapping(target = "bookingCalendar", ignore = true)
    public abstract RoomDTO roomDOToRoomDTO(RoomDO roomDO);

    @AfterMapping
    void setBookingCalendar(RoomDO roomDO, @MappingTarget RoomDTO roomDTO) {
        List<LocalDate> bookingDatesEndDateExclusive = roomDO.getBookings().stream()
                .map(BookingDO::getBookingInterval)
                .flatMap(this::getDayList).collect(Collectors.toList());

        roomDTO.setBookingCalendar(bookingDatesEndDateExclusive);
    }

    Stream<LocalDate> getDayList(Range<LocalDate> range) {
        LocalDate startDate = range.lower();
        LocalDate endDate = range.upper();
        long numOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate);

        return Stream.iterate(startDate, date -> date.plusDays(1))
                .limit(numOfDaysBetween);
    }
}
