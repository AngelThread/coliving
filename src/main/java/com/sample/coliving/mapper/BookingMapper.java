package com.sample.coliving.mapper;

import com.sample.coliving.controller.dto.BookingDTO;
import com.sample.coliving.controller.entity.BookingDO;
import com.vladmihalcea.hibernate.type.range.Range;
import org.mapstruct.*;

import java.time.LocalDate;

@Mapper(componentModel = "spring")
public abstract class BookingMapper {

    @Mapping(source = "bookingDTO", target = "bookingInterval", qualifiedByName = "bookingIntervalMapping")
    @Mapping(target = "id", ignore = true)
    public abstract BookingDO bookingDTOToBookingDo(BookingDTO bookingDTO);

    @Mapping(target = "startDate", ignore = true)
    @Mapping(target = "endDate", ignore = true)
    public abstract BookingDTO bookingDOToBookingDTO(BookingDO bookingDO);


    @Named("bookingIntervalMapping")
    Range<LocalDate> locationToLocationDto(BookingDTO bookingDTO) {

        return Range.closedOpen(
                bookingDTO.getStartDate(),
                bookingDTO.getEndDate()
        );
    }

    @AfterMapping
    protected void setStartAndEndDate(BookingDO bookingDO, @MappingTarget BookingDTO bookingDTO) {
        LocalDate startDate = bookingDO.getBookingInterval().lower();
        bookingDTO.setStartDate(startDate);
        LocalDate endDate = bookingDO.getBookingInterval().upper();
        bookingDTO.setEndDate(endDate);
    }
}
