package com.sample.coliving.service;

import com.sample.coliving.controller.entity.BookingDO;
import com.sample.coliving.exception.EntityNotFoundException;
import com.sample.coliving.exception.RoomAlreadyTakenException;

import java.util.List;

public interface BookingService {
    BookingDO createBooking(BookingDO booking) throws RoomAlreadyTakenException;

    List<BookingDO> getAllBookings();

    void deleteBooking(long bookingId) throws EntityNotFoundException;

    BookingDO findBooking(long bookingId) throws EntityNotFoundException;
}
