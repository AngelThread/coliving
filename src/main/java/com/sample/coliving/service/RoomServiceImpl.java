package com.sample.coliving.service;

import com.sample.coliving.controller.entity.RoomDO;
import com.sample.coliving.exception.EntityNotFoundException;
import com.sample.coliving.repository.RoomRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class RoomServiceImpl implements RoomService {

    @Autowired
    private final RoomRepository roomRepository;


    @Override
    public List<RoomDO> getAllRooms() {
        List<RoomDO> allRoomsWithBooking = roomRepository.findAllRoomsWithBooking();

        return allRoomsWithBooking;
    }

    @Override
    public RoomDO createRoom(RoomDO roomDO) {
        return roomRepository.save(roomDO);
    }

    @Override
    public RoomDO getRoom(long roomId) throws EntityNotFoundException {
        return roomRepository.findById(roomId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("There is no room with id:%d", roomId)));
    }
}

