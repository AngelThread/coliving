package com.sample.coliving.service;

import com.sample.coliving.controller.entity.RoomDO;
import com.sample.coliving.exception.EntityNotFoundException;

import java.util.List;

public interface RoomService {
    List<RoomDO> getAllRooms();

    RoomDO createRoom(RoomDO roomDO);

    RoomDO getRoom(long roomId) throws EntityNotFoundException;

}
