package com.sample.coliving.service;

import com.sample.coliving.controller.entity.BookingDO;
import com.sample.coliving.exception.EntityNotFoundException;
import com.sample.coliving.exception.RoomAlreadyTakenException;
import com.sample.coliving.repository.BookingRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class BookingServiceImpl implements BookingService {

    @Autowired
    private final BookingRepository bookingRepository;

    @Override
    public BookingDO createBooking(BookingDO booking) throws RoomAlreadyTakenException {
        BookingDO createdBooking = null;
        try {
            createdBooking = bookingRepository.save(booking);
        } catch (DataIntegrityViolationException ex) {
            throw new RoomAlreadyTakenException(ex);
        }
        return createdBooking;
    }

    @Override
    public List<BookingDO> getAllBookings() {
        return bookingRepository.findAll();
    }

    @Override
    public void deleteBooking(long bookingId) throws EntityNotFoundException {

        BookingDO bookingToBeDeleted = bookingRepository.findById(bookingId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("There is no booking with this id:%d", bookingId)));
        bookingRepository.delete(bookingToBeDeleted);
    }

    @Override
    public BookingDO findBooking(long bookingId) throws EntityNotFoundException {
        return bookingRepository.findById(bookingId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("There is no booking with this id:%d", bookingId)));
    }
}
