package com.sample.coliving.repository;

import com.sample.coliving.controller.entity.BookingDO;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingRepository extends JpaRepository<BookingDO, Long> {
}
