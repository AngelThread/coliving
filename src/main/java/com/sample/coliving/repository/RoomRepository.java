package com.sample.coliving.repository;

import com.sample.coliving.controller.entity.RoomDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RoomRepository extends JpaRepository<RoomDO,Long> {

    @Query(value = "SELECT p FROM RoomDO p LEFT JOIN FETCH p.bookings")
    List<RoomDO> findAllRoomsWithBooking();
}
