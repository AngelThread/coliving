package com.sample.coliving;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ColivingApplication {

    public static void main(String[] args) {
        SpringApplication.run(ColivingApplication.class, args);
    }

}
