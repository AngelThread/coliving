package com.sample.coliving.exception;

public class RoomAlreadyTakenException extends Exception {
    public RoomAlreadyTakenException(Throwable throwable) {
        super(throwable);
    }
}
